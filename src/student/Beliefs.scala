package student

import collection.mutable.{Map => MMap, Set => MSet}
import BaseAction._
import mas.agents.task.mining.StatusMessage

import scala.util.Random
import scala.annotation.tailrec

/**
  * Created by okubis on 10/24/17.
  */
case class Beliefs(myself: Int) {
  import Beliefs._
  // beliefs about world
  val rnd = new Random(myself)
  var obstacles:MSet[(Int,Int)] = MSet()            // where the obstacles are
  var gold:MSet[(Int,Int)]  = MSet()                // where the gold is
  var depots:MSet[(Int,Int)] = MSet()               // where depots are
  var unknown:MSet[VoidObj] = MSet()                // HIC SVNT DRACONES
  var agentLocations:MMap[Int,OtherAgent] = MMap()  // last know position of others
  var blockedAgents:MSet[Int] = MSet()              // blocked agents
  var loaded:Boolean = false                        // am I loaded
  var supported:Boolean = false                     // do I have soppurt for gold picking
  private var blocked:Boolean = false               // Am I in able to step aside
  var status: StatusMessage = _                     // my status
  lazy val limits:(Int,Int) = (status.width,status.height)

  def alterEgo():OtherAgent = agentLocations(myself)
  import scala.collection.JavaConverters._
  def sensorReadings: List[StatusMessage.SensorData] = status.sensorInput.asScala.toList

  def updateStatus(newStatus: StatusMessage):Unit = {
    status = newStatus
    agentLocations.put(myself,OtherAgent(status.agentX,status.agentY))
  }

  // The best way to treat obstacles is to use them as stepping-stones. Laugh at them, tread on them, and let them lead you to something better. ― Enid Blyton
  def updateObstacles(o:Obstacle):Unit = {
    unknown -= VoidObj(o.x,o.y)
    obstacles += ((o.x,o.y))
  }

  // Even I fell into the most terrible of human traps... Trying to change what is already past. -- Dr. Robert Ford
  def updateGold(g:Gold,add:Boolean):Unit = {
    if(add)
      gold += ((g.x,g.y))
    else
      gold -= ((g.x,g.y))
    unknown -= VoidObj(g.x,g.y)
  }

  // depot, sweet depot.
  def updateDepots(d:Depot):Unit = {depots += d.coordinates; unknown -= VoidObj(d.x,d.y)}

  // chaos is a ladder. Yet, knowledge is power.
  def increaseKnowledge(s:Seen):Unit = unknown -= VoidObj(s)//VoidObj(s.x,s.y)

  // in God we trust, all other must be proven.
  def updateBelief(mo: MapObject): Unit ={
    mo match {
      // For all the
      case g:Gold => updateGold(g,add=true)
      case o:Obstacle => updateObstacles(o)
      case d:Depot => updateDepots(d)
      case s:Seen => increaseKnowledge(s); if(gold.contains(s.coordinates)) {gold -= s.coordinates}
      // of the nations are idols: but the LORD made the heavens.
      case oa:OtherAgent => increaseKnowledge(Seen(oa))
      case _ => Unit
    }
  }

  def updateBeliefs(percepts: List[MapObject]):Unit = {
    // here we update our beliefs.
    percepts.foreach(updateBelief)
  }

  def updateAgentCoordinates(coordinates:(Int,Int), id:Int = myself):Unit = agentLocations.put(id,OtherAgent(coordinates._1,coordinates._2))

  def mapFullyKnown:Boolean = unknown.isEmpty

  def distance(fromX:Int,fromY:Int)(toX:Int,toY:Int):Int = Math.abs(toX-fromX) + Math.abs(toY-fromY)
  def distance(from:(Int,Int))(to:(Int,Int)):Int = distance(from._1,from._2)(to._1,to._2)

  def nearestStatic[A <: MapObject](mo:A):(A,Int) = {

    val d = distance(mo.coordinates) _

    def nrst(l:List[(Int,Int)]): (VoidObj,Int) = {
      val (h,_) = (l.head,l.tail)
      val n = l.foldLeft((h,d(h)))((a,b)=> {val d2 = d(b);if(d2<a._2)(b,d2)else a})
      (VoidObj(n._1._1,n._1._2),n._2)
    }

    mo match {
      case Depot(_, _) =>
        val a = nrst(depots.toList)
        (Depot(a._1).asInstanceOf[A], a._2)
      case Obstacle(_, _) =>
        val a = nrst(obstacles.toList)
        (Obstacle(a._1).asInstanceOf[A], a._2)
      case Gold(_, _) =>
        val a = nrst(gold.toList)
        (Gold(a._1).asInstanceOf[A], a._2)
      case _ =>
        if (unknown.isEmpty) (VoidObj(rnd.nextInt(limits._1),rnd.nextInt(limits._2)).asInstanceOf[A],0)
        else {
          val a = nrst(unknown.toList.map(_.coordinates))
          (VoidObj(a._1).asInstanceOf[A], a._2)
        }
    }
  }
  /**
    * find an agent that is believed to be the nearest one
    * @param to  position to the seeking agent (x-axis,y-axis)
    * @return   other agents position, distance between the two, id of the nearest agent
    */
  def nearestAgent(to:(Int,Int)):(OtherAgent,Int,Int)= {
    val dist = distance(to) _
    val nrst = agentLocations.filterNot(_._1==myself).toList.map(e => (e._1,e._2,dist(e._2.coordinates)))
      .foldLeft((-1,OtherAgent(to._1+100,to._2+100),200))((a,b) => if(a._3<=b._3) a else b)
    (nrst._2,nrst._3,nrst._1)
  }

  /**
    * find an agent that is believed to be the nearest one
    * @param x  position to the seeking agent (x axis)
    * @param y  position to the seeking agent (y axis)
    * @return   other agents position, distance between the two, id of the nearest agent
    */
  def nearestAgent(x:Int,y:Int):(OtherAgent,Int,Int)= {
    nearestAgent((x,y))
  }

  def load():Unit = {supported = false; loaded = true}
  def unload():Unit = {supported = false; loaded = false}

  def block():Unit = {blocked = true}
  private def unblock():Unit = {blocked = false}
  def isBlocked:Boolean = blocked

  def toStep(f:(Int,Int),t:(Int,Int)):BaseAction = {
    //if(agentLocations.toList.map(_._2.coordinates).contains(t)){Console.err.println(s"/////////////////////////////////////////////////// $myself")Improvise}else
    if(f._1 < t._1)
      Right
    else if(f._1 > t._1)
      Left
    else if(f._2 < t._2)
      Down
    else
      Up
  }


  // The way we win matters  --Ender
  @throws[Unreachable]
  def A_*(f:(Int,Int),t:(Int,Int),agentID: Int,theoretical:Boolean = true):(BaseAction,List[(Int,Int)]) = {
    val gscore:MMap[(Int,Int),Int] = MMap((f,0))
    def fscore(checkpoint:(Int,Int)):Int = gscore(checkpoint) + distance(checkpoint)(t)

    val closedSet:MSet[(Int,Int)] = MSet()
    val openSet:MSet[(Int,Int)] = MSet(f)
    val prevNode:MMap[(Int,Int),(Int,Int)] = MMap()

    @tailrec
    def reconstructPath(at:(Int,Int),path:List[(Int,Int)]):(BaseAction,List[(Int,Int)]) = {
      if(at == f){
        path match{
          case Nil => (Left,path)
          case _ => (toStep(f,path.head),path)
        }
      }else{
        reconstructPath(prevNode(at),at::path)
      }
    }

    def myFilter(c:(Int,Int)):Boolean = !closedSet.contains(c) && !obstacles.contains(c) &&
      {if(theoretical) true else !blockedAgents.filterNot(x => x==myself || x==agentID).map(b=>agentLocations(b).coordinates).contains(c)} &&
      {if(agentID<0) true else c!=agentLocations(agentID).coordinates}

    def nCost(c:(Int,Int)):Int = {
      //val n = OtherAgent(c._1,c._2)
      1                                   // Jsem nejbohatsi z bank nad Seinou
    }

    @throws[Unreachable]
    @tailrec
    def search: (BaseAction,List[(Int,Int)]) = {
      if (openSet.isEmpty) {Console.err.println(myself+"Unreachable "+t);throw Unreachable(t)}       // The way is shut. It was made by those who are Dead. And the Dead keep it. The way is shut.
      val cur = openSet.toList.minBy(e => fscore(e))      // Until the time comes.
      if (cur == t) reconstructPath(cur,Nil)              // The sword was reforged
      else {
        // once you enter the game of thrones
        openSet -= cur // you win
        closedSet += cur // or you die

        val neigbours = Agent.nearbyArea4(cur._1)(cur._2)(limits).map(_.coordinates).filter(myFilter)
        openSet ++= neigbours
        neigbours.map(n => (n, gscore(cur) + nCost(n))).foreach(n => if (n._2 < gscore.getOrElse(n._1, Int.MaxValue)) {
          prevNode.put(n._1, cur)
          gscore.put(n._1, n._2)
        })
        search
      }
    }

    search
  }

  @throws[Unreachable]
  def stepTowards(from:(Int,Int) = (status.agentX,status.agentY),to:(Int,Int)): BaseAction = {
    val (step,_) = A_*(from,to,-1)
    step
  }

  private def entireMap:Set[(Int,Int)] = {
    (for{
      x <- 0 until status.width
      y <- 0 until status.height
    }yield (x,y)).toSet.diff(obstacles)
  }

  def reinitMap():Unit = {
    unknown ++= entireMap.map(x => VoidObj(x._1,x._2))
  }

  def mapReachable(hid:Int, theor1:Boolean=false, theor2:Boolean=true, l:List[(Int,Int)]):List[((Int,Int),(Int,Int))]={

    @throws[Unreachable]
    def astar(f:(Int,Int),t:(Int,Int),id:Int,theoretical:Boolean) = A_*(f,t,id,theoretical)
    l.flatMap(e => {
        try {
          //val d1 = astar(alterEgo().coordinates,e,hid,theor1)
          val d1 = astar(alterEgo().coordinates,e,-1,theor1)
          val d2 = astar(agentLocations(hid).coordinates,e,-1,theor2)
          Some((e,(d1._2.length,d2._2.length)))
        } catch {
          case u:Unreachable => u.printStackTrace();None
        }
      }
    )
  }


  /**
    * retrives the nearest spot for avoidance
    * @param going2 where the hack am I going to?
    * @param hisPath where the hack is He going to?
    * @return                         ((spot.x,spot.y), (myDistance,hisDistance), Am I blocked?)
    */
  def spot4Avoidance(going2:(Int,Int),hisId:Int,isHeBlocked:Boolean,hisPath:List[(Int,Int)]):((Int,Int),(Int,Int),Boolean) = {

    // This way. don't follow the lights. --Smeagol
    def safePath(f:(Int,Int),t:(Int,Int),id:Int):List[(Int,Int)]={
      try{
        A_*(alterEgo().coordinates,going2,-1)._2
      }catch{
        case u:Unreachable => u.printStackTrace();updateObstacles(Obstacle(u.x,u.y));List(f)
      }
    }

    val myPath = safePath(alterEgo().coordinates,going2,-1)
    val pathUnion:Set[(Int,Int)] = myPath.toSet ++ hisPath ++ Set(agentLocations(myself).coordinates)

    val spot:((Int,Int),(Int,Int))= safeMin(mapReachable(hisId,l=entireMap.diff(pathUnion).toList))
    if(spot._1==dummySpot) {
      block()
      val spot2 = safeMin(mapReachable(hisId,theor1 = true,l=entireMap.diff(pathUnion).toList))
      (spot2._1,spot2._2,blocked)
    }else{
      unblock()
      (spot._1,spot._2,blocked)
    }
  }

  def relative(ba:BaseAction):(Int,Int) = {
    ba match {
      case Left => (alterEgo().x - 1, alterEgo().y)
      case Right => (alterEgo().x + 1, alterEgo().y)
      case Up => (alterEgo().x, alterEgo().y-1)
      case Down => (alterEgo().x, alterEgo().y+1)
      case _ => alterEgo().coordinates
    }
  }

  def recomputeObstacles():Unit = {
    entireMap.foreach(e =>
      if (Agent.nearbyArea4(e._1)(e._2)(limits).forall(x => obstacles.contains(x.coordinates))) {obstacles.add(e);unknown-=VoidObj(e._1,e._2)})
  }

}

case object Beliefs{
  lazy val dummySpot:(Int,Int) = (-1,-1)
  lazy val strict = true

  val deafaultFilter:(Int,Int)=>Boolean = (_:Int,_:Int) => true

  def safeMin(l:List[((Int,Int),(Int,Int))]):((Int,Int),(Int,Int)) = {
    if (l.isEmpty) (dummySpot,dummySpot)
    else l.minBy(e => (e._2._1+e._2._1,e._1._1,e._1._2))
  }
}
