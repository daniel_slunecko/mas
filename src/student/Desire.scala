package student

import collection.mutable.{ListBuffer => MLB}
/**
  * Created by okubis on 10/21/17.
  */
sealed trait Desire{
  def toIntetions(beliefs: Beliefs): List[Intention]
}

// priority 0
case class MoveGold2Depot() extends Desire{
  override def toIntetions(beliefs: Beliefs): List[Intention] = {
    val l:MLB[Intention] = MLB()
    if(beliefs.depots.isEmpty) {
      l += FindDepot()
    }else
      l += Go2Depot()
    l.toList
  }
}
// priority 1
case class CollectGold() extends Desire{
  override def toIntetions(beliefs: Beliefs): List[Intention] = {
    val l:MLB[Intention] = MLB()
    if(beliefs.gold.isEmpty) {
      l += FindGold()
    }else
      l += PickSomeGold()
    l.toList
  }
}
// priority 2
case class ExploreMap() extends Desire{
  override def toIntetions(beliefs: Beliefs): List[Intention] = {
    val l:MLB[Intention] = MLB()
    if(beliefs.unknown.isEmpty)
      l += FindGold()
    else{
      l += Explore()
    }
    l.toList
  }
}


