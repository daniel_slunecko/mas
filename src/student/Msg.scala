package student

/**
  * Created by okubis on 10/21/17.
  */

abstract sealed class Msg() extends mas.agents.Message with Serializable
// gold lifting
case class HelpRequest(sender:Int,x:Int,y:Int) extends Msg
case class HelpRequestFinished(sender:Int,x:Int,y:Int) extends Msg
case class YouHaveMyHelp(atX:Int,atY:Int) extends Msg
// map exploration
case class MapUpdate(mo:MapObject) extends Msg
case class GoldUpdate(go:Gold,add:Boolean) extends Msg
// team awareness
case class TellPosition(id:Int,isBlocked:Boolean,otherAgent: OtherAgent) extends Msg
case class AskPosition(replyTo:Int) extends Msg
// movement coopearation
case class Itinerary(from:Int,isBlocked:Boolean,path:List[(Int,Int)]) extends Msg
//case class Proposition(spot:(Int,Int),distancesDiff,proposedBy:Int) extends Msg


