package student

/**
  * Created by okubis on 10/21/17.
  */
sealed abstract class MapObject(val x:Int = -1,val y:Int = -1) extends Serializable{
  def coordinates:(Int,Int) = (x,y)
}

case class Obstacle(override val x:Int,override val y:Int) extends MapObject(x,y)
case object Obstacle{def apply[A <:MapObject](a:A):Obstacle = apply(a.x,a.y)}
// I am a dwarf and I dig in the hole, diggy, diggy hole...
case class Gold(override val x:Int,override val y:Int) extends MapObject(x,y)
case object Gold{def apply[A <:MapObject](a:A):Gold = apply(a.x,a.y)}
case class Depot(override val x:Int,override val y:Int) extends MapObject(x,y)
case object Depot{def apply[A <:MapObject](a:A):Depot = apply(a.x,a.y)}
case class OtherAgent(override val x:Int,override val y:Int) extends MapObject(x,y)
case object OtherAgent{def apply[A <:MapObject](a:A):OtherAgent = apply(a.x,a.y)}
case class Seen(override val x:Int,override val y:Int) extends MapObject(x,y)
case object Seen{def apply[A <:MapObject](a:A):Seen = apply(a.x,a.y)}
case class VoidObj(override val x:Int,override val y:Int) extends MapObject(x,y)
case object VoidObj{def apply[A <:MapObject](a:A):VoidObj = apply(a.x,a.y)}

