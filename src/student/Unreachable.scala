package student

/**
  * Created by okubis on 10/27/17.
  */
case class Unreachable(coordinates:(Int,Int)) extends Exception{
  def x:Int = coordinates._1
  def y:Int = coordinates._2
}
