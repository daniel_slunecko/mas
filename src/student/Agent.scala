package student

import mas.agents.AbstractAgent
import mas.agents.Message
import mas.agents.SimulationApi
import mas.agents.StringMessage
import mas.agents.task.mining._
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

import BaseAction._

import collection.mutable.{ListBuffer => MLB, Map => MMap}
import scala.annotation.tailrec


@throws[IOException]
@throws[InterruptedException]
class Agent(val id: Int, val is: InputStream, val os: OutputStream, val api: SimulationApi) extends AbstractAgent(id, is, os, api) {

  // consciousness including beliefs about world
  val self = Self(Beliefs(id))

  // init map as completely unknown.
  lazy val initialized:Boolean = {initUnkown();true}

  // state
  lazy val mates:Set[Int] = Set(1,2,3,4) - id    // mates. check mates...

  def statePosition(sm:StatusMessage): String = s"""I am now on position [${sm.agentX},${sm.agentY}] of a ${sm.width}x${sm.height}."""

  /** map initialization method */
  def initUnkown():Unit = {
    println(id + " INIT INIT INIT INIT INIT INIT INIT INIT INIT INIT INIT INIT INIT")
    self.beliefs.updateStatus(sense())
    val xIndeces = 0 until self.beliefs.status.width
    val yIndeces = 0 until self.beliefs.status.height
    for{
      x <- xIndeces
      y <- yIndeces
    } self.beliefs.unknown += VoidObj(x,y)
  }

  def reinitUnkown():Unit = {
    println(id + " REINIT REINIT REINIT REINIT REINIT REINIT REINIT REINIT REINIT REINIT REINIT REINIT REINIT")
    self.beliefs.updateStatus(sense())
    self.beliefs.reinitMap()
  }


  /*
  They have mouths, but they speak not: eyes have they, but they see not:
  They have ears, but they hear not: noses have they, but they smell not:
  They have hands, but they handle not: feet have they, but they walk not:
  neither speak they through their throat.
  They that make them are like unto them; so is every one that trusteth in them.
   */
  /** check the surroundinds and get list of notable stuff */
  def checkPercepts:List[MapObject] = {
    def sensor2MapObjects(d: StatusMessage.SensorData):MapObject = {
      d.`type` match {
        case 1 => Obstacle(d.x, d.y)
        case 2 => Depot(d.x, d.y)
        case 3 => Gold(d.x, d.y)
        case 4 => OtherAgent(d.x, d.y)
        case _ => VoidObj(d.x, d.y)
      }
    }

    def firstTimeObservations(area: List[VoidObj]):List[Seen] ={
      area//.filter(self.beliefs.unknown.contains)
        .map(e => Seen(e.x,e.y))
    }

    // all places we observed at
    val (atX,atY) = self.perception.coordinates
    val nearbyArea:List[VoidObj] = Agent.nearbyArea8(atX)(atY)(self.beliefs.limits)
    val m:MMap[(Int,Int),MapObject] = {MMap()}
    // all locations we see for the 1st time
    firstTimeObservations(nearbyArea) foreach (e => m.put((e.x,e.y),e))

    // we filter only relevant observations
    for(d <- self.beliefs.sensorReadings.map(sensor2MapObjects)){
      d match {
        case Depot(x,y) => m.update((x,y),d)
        case Obstacle(x,y) => m.update((x,y),d)
        case Gold(x,y) => m.put((x,y),d)//if(!self.helpRequests.map(e => (e.x,e.y)).contains((x,y))) m.put((x,y),d)
        case OtherAgent(x,y) => m.put((x,y),d)
        case _ => Console.err.println(s"PERCEPTION_ERROR: $d")
      }
    }

    m.values.toList
  }

  // "It's not about money. It's about sending a message. Everything burns!" - Joker
  /** incomming messages */
  def checkMailbox:List[Message] = {
    def ordF(m:Message)={m match{
      case _:YouHaveMyHelp => 0
      case _:HelpRequestFinished => 1
      case _:HelpRequest => 2
      case _:GoldUpdate => 3
      case _:MapUpdate => 4
      case _:TellPosition => 5
      case _:Itinerary => 6
    }}

    val lb = MLB[Message]()
    while (messageAvailable){
        val m: Message = readMessage
        lb += m
    }
    lb.toList.sortBy(ordF)
  }

  /** react to message */
  def processMessage(m: Message):Any = {
    m match {
      // gold lifting --------------------------------------------------------------------------------------------------
      case HelpRequest(from,x,y) =>
        if(self.proposeCommitment(Help2Lift(from,x,y))){
            verify
            self replan()
        }
      case f:HelpRequestFinished =>
        if(self.softPlans.head == Help(f.sender,f.x,f.y)) {
          self commitmentFinnished Help2Lift(f.sender,f.x,f.y)
          self planFinnished self.getPlan//plan
          verify
          self replan()
        }
      case YouHaveMyHelp(a,b) => if(self.perception.coordinates == (a,b)){
        execute(Pick)
      }
      // map exploration -----------------------------------------------------------------------------------------------
      case m:MapUpdate =>
        m.mo match {
          case Seen(x,y) if self.beliefs.gold.contains((x,y)) =>
            self.beliefs.updateBelief(m.mo)
            verify
            self replan()
          case _ =>
            self.beliefs.updateBelief(m.mo)
        }
      case GoldUpdate(gold,true) =>
        self.beliefs.updateGold(gold,add=true)
        self planFinnished self.getPlan//plan
        verify
        self replan()
      case GoldUpdate(gold,false) =>
        self.beliefs.updateGold(gold,add=false)
        //verify
        self planFinnished self.getPlan//plan
        verify
        self replan()
      // team awareness ------------------------------------------------------------------------------------------------
      case a:AskPosition => sendMessage(a.replyTo,TellPosition(id,self.beliefs.isBlocked,self.perception))
      case t:TellPosition =>
        self.beliefs.agentLocations.put(t.id,t.otherAgent)
        if (t.isBlocked) self.beliefs.blockedAgents.add(t.id)
        else self.beliefs.blockedAgents.remove(t.id)
      // cooperation ---------------------------------------------------------------------------------------------------
      case i:Itinerary =>
        // if cannot be interrupted => ignore
        if ({
          self.getPlan match {
            case Help(_,x,y) if Agent.nearbyArea4(x)(y)(self.beliefs.limits).contains(VoidObj(self.perception))=> true;
            //case RetrieveGoldFrom(x1,y1) if self.perception == OtherAgent(x1, y1) =>
              //sendMessage(i.from,HelpRequest(self.beliefs.myself,x1,y1))
              //execute(Pick)
              true
            //case p if !BaseAction.isMove(p.nextAction(self.beliefs)) => true
            case _ => false
          }
        }) {
          self.beliefs.block()
          sendMessage(i.from,TellPosition(id,self.beliefs.isBlocked,self.perception))
          /*
          if(self.getPlan.nextAction(self.beliefs)==Break){ execute(Break)}
          Console.err.println(s"$id |||||||||||||| MY PLAN IS: ${self.getPlan} + ${self.getPlan.nextAction(self.beliefs)} AND THUS I'M NOT GONNA MOVE.")
          self.beliefs.block()
          sendMessage(i.from,TellPosition(id,self.beliefs.isBlocked,self.perception))
          verify
          self replan()
          */

          // else if sender is in my way, resolve
        }else if (self.getItinerary.contains(self.beliefs.agentLocations(i.from).coordinates)){
          val ((x, y), (myDist, hisDist), blocked) = self.beliefs.spot4Avoidance(self.getDestination, i.from, i.isBlocked, i.path)

          // if I can move and I am closer or He's blocked or the distance is the same and I have higher ID,
          // I try to commit to move
          if(!blocked && (myDist<hisDist || (myDist == hisDist && id>i.from) || i.isBlocked) && self.proposeCommitment(Evade(i.from, x, y))){
            Console.err.println(id +" "+ Evade(i.from,x,y).toString + i.path.toString())
            verify
            self replan()

          // else if I am blocked, and he is blocked, send others to move from our paths
          }else if(blocked && i.isBlocked){
            Console.err.println(s"$id IS BLOCKED, I'M NOT GONNA MOVE.")
            val l = self.beliefs.agentLocations.filter(_._1!=id).filter(x => x._2.coordinates == self.beliefs.relative(self.getPlan.nextAction(self.beliefs)))
            if(l.nonEmpty){
              Console.err.println(s"$id ASKS ${l.toString()} TO MOVE.")
              l.foreach(x => sendMessage(x._1,Itinerary(id,self.beliefs.isBlocked,i.path++self.getItinerary)))
            }

          // else if he is supposed to step aside and not blocked
          }else if(!i.isBlocked){
              Console.err.println(s"$id IS BLOCKED, BUT ${i.from} IS NOT")
              sendMessage(i.from,Itinerary(id,self.beliefs.isBlocked,self.getItinerary))
          }else{
            Console.err.println(s"AGENT $id FAILED TO COMMIT TO EVADE")
          }

        // else if this agent is not in my way
        }else if (!self.getItinerary.contains(self.beliefs.agentLocations(i.from).coordinates)){
          Console.err.println(s"$id IS NOT IN DIRRECT LOCK WITH ${i.from} AS HE IS GOING TO: ${self.getPlan.nextAction(self.beliefs)} FROM ${self.commitments.head}")
          if(self.getPlan match {case RetrieveGoldFrom(_,_)=>true; case _ => false}){
            Console.err.println(s"=================================  $id  ===============================")
            sendMessage(i.from,HelpRequest(id,self.perception.x,self.perception.y))
            if(self.getPlan.nextAction(self.beliefs) == Sense){
              self.beliefs.block()
              sendMessage(i.from,TellPosition(id,self.beliefs.isBlocked,self.perception))
              sendMessage(i.from,HelpRequest(id,self.perception.x,self.perception.y))
              verify
              self replan()
              execute(Break)
            }
          }
          if(self.getPlan.nextAction(self.beliefs)==Break){
            execute(Break)
            val ((x, y), (myDist, hisDist), blocked) = self.beliefs.spot4Avoidance(self.getDestination, i.from, i.isBlocked, i.path)
            if (self.proposeCommitment(Evade(i.from, x, y))) {
              Console.err.println(id + " " + Evade(i.from, x, y).toString + i.path.toString())
              verify
              self replan()
            }
          }

          // else if some other agent is in my way, ask him to get out from our combined trajectories
          val l = self.beliefs.agentLocations.filter(x => self.getItinerary.contains(x._2.coordinates))
          //self.beliefs.agentLocations.filter(x => x._2.coordinates == self.beliefs.relative(self.getPlan.nextAction(self.beliefs)))
          if(l.nonEmpty){
            l.foreach(x => sendMessage(x._1,Itinerary(id,self.beliefs.isBlocked,i.path++self.getItinerary)))
          }
          verify
          self replan()

          // otherwise I can proceed with my plan, since I will move from his path
        }else{
          println("HELLO DARKNESS MY OLD FRIEND")
        }
      // other
      case s:StringMessage => println(s.content)
      case _ => Unit
    }
  }

  def shareKnowledge(objects: List[MapObject]): Unit ={
    val msgs:List[Msg] = TellPosition(id,self.beliefs.isBlocked,self.perception)::objects.map(MapUpdate)
    for{
      agent <- mates
      msg <- msgs
    } sendMessage(agent,msg)
  }

  def friendAt(x:Int,y:Int):(Boolean,Int) = {
    val who:Option[(Int,OtherAgent)] = self.beliefs.agentLocations.toList.find(e => e._2 == OtherAgent(x,y))
    if (who.isDefined) (true,who.get._1) else (false,0)
  }
  def frinedAt(c:(Int,Int)):(Boolean,Int) = friendAt(c._1,c._2)

  def execute(a: BaseAction): StatusMessage = {
    lazy val moves = BaseAction.values filter BaseAction.isMove
    val friend = frinedAt(self.beliefs.relative(a))
    a match {
      case Left =>
        if (friend._1){
          //Thread.sleep(self.beliefs.rnd.nextInt(5));
          sendMessage(friend._2,Itinerary(self.beliefs.myself,self.beliefs.isBlocked,self.getItinerary))
          sense
        } else
        left
      case Right =>
        if (friend._1){
          //Thread.sleep(self.beliefs.rnd.nextInt(5));
          sendMessage(friend._2,Itinerary(self.beliefs.myself,self.beliefs.isBlocked,self.getItinerary))
          sense
        }else
        right
      case Up =>
        if (friend._1){
          //Thread.sleep(self.beliefs.rnd.nextInt(5));
          sendMessage(friend._2,Itinerary(self.beliefs.myself,self.beliefs.isBlocked,self.getItinerary))
          sense
        } else
        up
      case Down =>
        if (friend._1){
          //Thread.sleep(self.beliefs.rnd.nextInt(5));
          sendMessage(friend._2,Itinerary(self.beliefs.myself,self.beliefs.isBlocked,self.getItinerary))
          sense
        } else
        down
      case Pick =>
        self.beliefs.load()
        val p = pick
        mates.foreach(agent => sendMessage(agent,HelpRequestFinished(id,self.perception.x,self.perception.y)))
        p
      case Drop =>
        self.beliefs.unload()
        if(self.beliefs.gold.isEmpty) {reinitUnkown();self planFinnished self.getPlan;verify;self replan()}
        drop
      case Sense =>
        self planFinnished self.getPlan
        verify
        self replan()
        sense
      case Improvise =>
        if(self.beliefs.gold.isEmpty) {
          reinitUnkown()
          verify
          self replan()
        }
        execute((moves drop self.beliefs.rnd.nextInt(moves.size)).head)
      case Break =>
        val s = sense
        self planFinnished self.getPlan
        verify
        self replan()
        s
    }
  }

  def seek(p:Plan):StatusMessage = {
    val nact = p.nextAction(self.beliefs)

    execute(nact)
  }
  def help(h:Help):StatusMessage = {
   val nact = h.nextAction(self.beliefs)
    if(nact == Sense) {sendMessage(h.whom,YouHaveMyHelp(h.x,h.y)); System.err.println(s"$id, ${self.perception.coordinates}, $h")}
    execute(nact)
  }
  def move(p:Plan):StatusMessage = {
    execute(p.nextAction(self.beliefs))
  }

  def retrieve(p:Plan):StatusMessage = {
    val nact = p.nextAction(self.beliefs)
    val at = self.perception
    nact match{
      case Sense =>
        val mate = self.beliefs.nearestAgent(at.coordinates)
        if(self.beliefs.status.isAtGold){
          sendMessage(mate._3,HelpRequest(self.beliefs.myself,at.x,at.y))
          execute(Sense)
        }else {
          verify
          self replan()
          execute(Break)
        }
      case Break =>
        execute(Sense)
      case Pick =>
        val _ = execute(Pick)
        self.beliefs.load()
        self.beliefs.updateGold(Gold(at),add=false)
        mates.foreach(agent => sendMessage(agent,GoldUpdate(Gold(at),add=false)))
        mates.foreach(agent => sendMessage(agent,HelpRequestFinished(id,at.x,at.y)))
        Console.err.println(s"I $id TOLD AGENTS TO DROP HELP")
        execute(Break)
      case _ => execute(nact)
    }
  }



  def proceedWith(p:Plan):StatusMessage = {
    p match{
      case Seek4(_) => seek(p)
      case ShiftFrontier(_,_) => seek(p)
      case h:Help => help(h)
      case Move2(_,_) => move(p)
      case RetrieveGoldFrom(_,_) => retrieve(p)
      case _ =>
        Console.err.println(s"ERORR: $id can't do ${p.toString}")
        sense()
    }
  }

  def verify:Boolean = {
    if(self.beliefs.mapFullyKnown && self.beliefs.gold.isEmpty) {reinitUnkown()}
    val options:List[Intention] = self.filterDesires()            // What do you believe in? I believe that whatever doesn't kill you, simply makes you stranger
    val commitmentAdded = self.proposeCommitments(options).foldLeft(false)((x,y)=>x||y)
    val commitmentDroped = self.reconsiderCommitments()
    commitmentAdded||commitmentDroped
  }

  @tailrec
  private def loop():Unit = {
    //println(s"agent:${id} is staying on his loop")
    val percepts:List[MapObject] = checkPercepts    // Have you ever felt any inconsistencies in this world?
    val msgs:List[Message] = checkMailbox           // Would the things I told you change the way you think about the newcomers, Dolores?
    self.beliefs.updateBeliefs(percepts)            // You don't see it, because you want to be fooled
    shareKnowledge(percepts)
    msgs.foreach(processMessage)                    // hear me roar!
    if(verify){
      self.replan()
    }
    self.beliefs.updateStatus(proceedWith(self.getPlan))

    // stay on the
    loop()
  }

  @throws[Exception]
    def act() {

          // update desires
          if (!initialized) {}
          self.realizeDesires() // upload the reveries

          // wake up Dolores!
          self.beliefs.updateStatus(sense())

          // say hello to other
          mates.foreach(sendMessage(_,TellPosition(id,self.beliefs.isBlocked,self.perception)))

          // every life has routines...
          loop()
  }
}

object Agent {
  lazy val neighbourhood8 = List((-1,-1),(0,-1),(1,-1),(-1,0),(0,0),(1,0),(-1,1),(0,1),(1,1))
  lazy val neighbourhood4 = List((0,-1),(-1,0),(1,0),(0,1))
  private def inTheWorld(thing: VoidObj)(limits:(Int,Int)):Boolean =               // it doesn't lok like anything to me
    thing.x > -1 && thing.y > -1 && thing.x < limits._1 && thing.y < limits._2
  def nearbyArea8(x:Int)(y:Int)(limits:(Int,Int)):List[VoidObj] =
    neighbourhood8.map(spot => VoidObj(spot._1+x,spot._2+y)).filter(inTheWorld(_)(limits))
  def nearbyArea4(x:Int)(y:Int)(limits:(Int,Int)):List[VoidObj] =
    neighbourhood4.map(spot => VoidObj(spot._1+x,spot._2+y)).filter(inTheWorld(_)(limits))
}