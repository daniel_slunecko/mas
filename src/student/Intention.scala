package student

/**
  * Created by okubis on 10/21/17.
  */
sealed trait Intention{
  def toPlan(beliefs: Beliefs): List[Plan]        // You had plans. You were a schemer --Joker
  lazy val priority:Int = ???                       // priority? what is such an abstract term good for?
}
// gold lifting
/**
  * Intent to help to a friend
  * @param to whom to help
  * @param atX x-coordinate
  * @param atY y-coordinate
  */
case class Help2Lift(to:Int,atX:Int,atY:Int) extends Intention{
  override def toPlan(beliefs: Beliefs): List[Plan] = List(Help(to,atX,atY))
  override lazy val priority = 0
}

// I said evade. EVADE, not invade...
/**
  * Intent to make way for some friend
  */
case class Evade(to:Int, atX:Int, atY:Int,prior: Int = -(System.currentTimeMillis() - Evade.start).toInt) extends Intention{
  override def toPlan(beliefs: Beliefs): List[Plan] = {
    List(Move2(atX,atY))
  }
  override lazy val priority:Int = prior
}
case object Evade{
  lazy val start:Long = System.currentTimeMillis()
}

// explore unknown and slay dragons
/**
  * go to the outskirts of the known world and seek for an adventure
  */
case class Explore() extends Intention{
  override def toPlan(beliefs: Beliefs): List[Plan] = {
    //Console.err.println(beliefs.alterEgo(),beliefs.status.agentX,beliefs.status.agentY)
    if (beliefs.mapFullyKnown) Nil else List(ShiftFrontier(VoidObj(beliefs.alterEgo()), beliefs.unknown.size))
  }
          //val n = beliefs.nearestStatic(VoidObj(beliefs.agentLocations(beliefs.myself)))
    override lazy val priority = 7//2
}
// co delat ze mas? navzdory otcum a synum, se dohodlat k cinum, kde domov je nas... -- K.Kryl
/**
  * where the hack should we put all this sh-gold. I mean gold
  */
case class FindDepot() extends Intention{
  override def toPlan(beliefs: Beliefs): List[Plan] = List(Seek4(Depot(beliefs.alterEgo())))
  override lazy val priority = 3
}
// bez domu Ivane.
/**
  * well you go 2 depot 2, you gold-digging robot.
  */
case class Go2Depot() extends Intention {
  override def toPlan(beliefs: Beliefs): List[Plan] = {
    List(Move2(beliefs.nearestStatic(Depot(beliefs.alterEgo()))._1.coordinates))
  }
  override lazy val priority = 4
}
// I am a dwarf and I dig in the hole, diggy diggy hole.
/**
  * There is no way into the mountain. Or is there?
  */
case class FindGold() extends Intention{
  override def toPlan(beliefs: Beliefs): List[Plan] = List(Seek4(Gold(beliefs.alterEgo())))
  override lazy val priority = 5
}
// my precioussss
/**
  * Intent to collect the nice, shiny thing.
  */
case class PickSomeGold() extends Intention{
  override def toPlan(beliefs: Beliefs): List[Plan] = {
    List(RetrieveGoldFrom(beliefs.nearestStatic(Gold(beliefs.alterEgo()))._1.coordinates))
  }
  override lazy val priority = 6
}