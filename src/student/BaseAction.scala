package student

/**
  * Created by okubis on 10/21/17.
  */
object BaseAction extends Enumeration {
  type BaseAction = Value
  val Left,Right,Up,Down,Pick,Drop,Sense,Break,Improvise = Value

  def isMove(a: BaseAction):Boolean = a == Left || a == Right || a == Up || a == Down
  def isBreak(a: BaseAction):Boolean = a == Break
  def isInternal(a:BaseAction):Boolean = a == Break || a == Improvise

}



