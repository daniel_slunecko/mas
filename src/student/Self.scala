package student

import collection.mutable.{ListBuffer => MLB,Set => MSet}

/**
  * Created by okubis on 10/25/17.
  */
case class Self(beliefs: Beliefs) {
  //todo make value, or make Set
  lazy val desires: MSet[Desire] = MSet(MoveGold2Depot(),CollectGold(),ExploreMap())
  //val intentions: MSet[Intention] = MSet()
  val commitments: MSet[Intention] = MSet()
  var softPlans: MLB[Plan] = MLB()
  //val helpRequests: MSet[HelpRequest] = MSet()

  def perception:OtherAgent = beliefs.alterEgo()

  /*
  All of us also lived among them at one time,
  gratifying the cravings of our flesh and following its desires and thoughts.
  Like the rest, we were by nature deserving of wrath.
   */
  def realizeDesires(): Unit = desires

  // remember Dolores!
  private def addIdea(idea: Any):Unit = {
    idea match {
      case d:Desire => desires += d
      case i:Intention => commitments += i
      case p:Plan => softPlans += p
      case _ => Unit
    }
  }

  // What is the most resilient parasite? Bacteria? A virus? An intestinal worm? An idea. Resilient... highly contagious.
  private def removeIdea(idea: Any):Unit = {
    idea match {
      case d:Desire => desires -= d
      case i:Intention => commitments -= i
      case p:Plan => softPlans -= p
      case _ => Unit
    }
  }

  // A creative man is motivated by the desire to achieve, not by the desire to beat others. Ayn Rand
  /**
    * we decide which desire should be embraced and which tu suppress
    * @return intentions
    */
  def filterDesires():List[Intention] = {
    //todo: modify if needed
    if(beliefs.loaded)
      MoveGold2Depot() toIntetions beliefs
    else if(beliefs.gold.isEmpty)
      ExploreMap() toIntetions beliefs
    else
      CollectGold() toIntetions beliefs
  }

  /**
    * drop pointless, infeasible and other bad commitments
    * @return changed?
    */
  def reconsiderCommitments(): Boolean = {
    val invalidCommitments: MSet[Intention] = MSet()
    commitments.foreach(
      {
        //case Help2Lift(to,atX,atY) if !helpRequests.contains(HelpRequest(to,atX,atY)) => invalidCommitments += Help2Lift(to,atX,atY)
        case Help2Lift(to,atX,atY) if !beliefs.gold.contains((atX,atY)) || beliefs.agentLocations(to).coordinates!=(atX,atY) => invalidCommitments += Help2Lift(to,atX,atY)
        case Evade(to,x,y,time) if perception.coordinates == (x,y) || beliefs.obstacles.contains((x,y)) => invalidCommitments += Evade(to,x,y,time)
        case Explore() if beliefs.mapFullyKnown => invalidCommitments += Explore()
        case FindDepot() if beliefs.depots.nonEmpty => invalidCommitments += FindDepot()
        case Go2Depot() if !beliefs.loaded => invalidCommitments += Go2Depot()
        case FindGold() if beliefs.gold.nonEmpty => invalidCommitments += FindGold()
        case PickSomeGold() if beliefs.loaded || beliefs.gold.isEmpty => invalidCommitments += PickSomeGold()
        case _ => Unit
      }
    )
    commitments --= invalidCommitments
    invalidCommitments.nonEmpty
  }

  def proposeCommitment(proposal:Intention):Boolean = {
    proposal match {
      case h:Help2Lift  =>
       if (!commitments.contains(h)) {commitments += proposal;true} else false
      case e:Evade => if (!commitments.contains(e)) {commitments += e; true} else false
        //commitments --= commitments.filterNot({case ev:Evade => true; case _ => false})
        //commitments += e
        //true
      case Explore() =>
        if (commitments.contains(Explore()) || beliefs.mapFullyKnown) false else{commitments += proposal; true}
      case FindDepot() =>
        if(beliefs.depots.isEmpty){commitments += proposal; true} else false
      case Go2Depot() =>
        if(commitments.contains(Go2Depot())) false else{commitments += proposal; true}
      case FindGold() =>
        if(beliefs.gold.isEmpty){commitments += proposal; true} else false
      case PickSomeGold() => if(commitments.contains(PickSomeGold()) || beliefs.loaded) false else {commitments += proposal; true}
    }
  }

  def proposeCommitments(newCommits: List[Intention]): List[Boolean] = {
    newCommits.map(proposeCommitment)
  }

  /**
    * turn commitments into plans.
    * @return true, if the first plan remains the same
    */
  def replan():Boolean = {
    if(beliefs.supported){beliefs.supported = false}

    // true if keep
    def planFilter(p:Plan):Boolean = {
      p match{
        case Seek4(Gold(_,_)) => beliefs.gold.isEmpty
        case Seek4(Depot(_,_)) => beliefs.depots.isEmpty
        case Seek4(_) => false
        case ShiftFrontier(VoidObj(_,_),n) => beliefs.unknown.size >= n && beliefs.unknown.nonEmpty
        case Help(to,x,y) => beliefs.agentLocations(to).coordinates == (x,y) && beliefs.gold.contains((x,y))
        case Move2(x,y) => !beliefs.obstacles.contains((x,y))
        case RetrieveGoldFrom(x,y) => beliefs.gold.contains((x,y)) && !beliefs.loaded
      }
    }

    if(softPlans.isEmpty){
      softPlans = MLB() ++= commitments.toList.sortBy(_.priority).flatMap(_.toPlan(beliefs))
      softPlans.filter(planFilter)
      false
    }
    else {
      val plan = softPlans.head
      softPlans = MLB() ++= commitments.toList.sortBy(_.priority).flatMap(_.toPlan(beliefs))
      softPlans.filter(planFilter)
      softPlans.head == plan
    }
  }

  def commitmentFinnished(c:Intention):Unit={
    removeIdea(c)
  }

  def planFinnished(p:Plan):Unit = {
    removeIdea(p)
  }

  def getPlan:Plan = softPlans.head

  def getDestination:(Int,Int) = {
    getPlan match {
      case Move2(x, y) => (x, y)
      case RetrieveGoldFrom(x, y) => (x, y)
      case Help(_, x, y) => (x, y)
      case Seek4(mo: MapObject) =>
        val nearestFree: (VoidObj, Int) = beliefs.nearestStatic(VoidObj(mo))
        nearestFree._1.coordinates
      case ShiftFrontier(mo: MapObject, _) =>
        val nearestFree: (VoidObj, Int) = beliefs.nearestStatic(VoidObj(mo))
        nearestFree._1.coordinates
    }
  }

  def getItinerary:List[(Int,Int)] = {
    try {
      beliefs.A_*(perception.coordinates, getDestination, -1)._2
    }catch{
      case u:Unreachable =>
        Console.err.println(beliefs.myself + " HERE IS THE BUG: "+ getPlan.toString)
        beliefs.updateObstacles(Obstacle(u.x,u.y))
        planFinnished(getPlan)
        val options:List[Intention] = filterDesires()            // What do you believe in? I believe that whatever doesn't kill you, simply makes you stranger
        proposeCommitments(options).foldLeft(false)((x,y)=>x||y)
        reconsiderCommitments()
        replan()
        getItinerary
    }
  }

}
