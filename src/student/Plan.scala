package student

import BaseAction._

/**
  * Created by okubis on 10/21/17.
  */
sealed trait Plan{                  // Do I really look like someone who has a plan? I'm just a dog chasing cars --Joker
  def nextAction(beliefs: Beliefs):BaseAction
}

case class Seek4(x: MapObject) extends Plan{
  override def nextAction(beliefs: Beliefs): BaseAction = {
    x match {
      case g:Gold if beliefs.gold.isEmpty => ShiftFrontier(g).nextAction(beliefs)
      case d:Depot if beliefs.depots.isEmpty => ShiftFrontier(d).nextAction(beliefs)
      case _ => Break
    }
  }
}
case class ShiftFrontier(mo:MapObject, n:Int=0) extends Plan{
  override def nextAction(beliefs: Beliefs): BaseAction = {
    val (whiteSpot,_): (VoidObj,Int) = beliefs.nearestStatic(VoidObj(mo))
    mo match {
      case Depot(_,_) =>
        if(beliefs.depots.nonEmpty) Break
        else {
          Move2(whiteSpot.coordinates).nextAction(beliefs)
        }
      case Gold(_,_) =>
        if(beliefs.gold.nonEmpty) Break
        else {
          Move2(whiteSpot.coordinates).nextAction(beliefs)
        }
      case VoidObj(_,_) =>
        if(beliefs.unknown.size < n || !beliefs.unknown.contains(whiteSpot)){
          Break
        }else {
          Move2(whiteSpot.coordinates).nextAction(beliefs)
        }
      case _ => Break
    }
  }
}
case class Help(whom: Int, x: Int, y: Int) extends Plan{
  lazy val places:((Int, Int)) => scala.List[VoidObj] = Agent.nearbyArea4(x)(y)            // this agent is going places
  override def nextAction(beliefs: Beliefs): BaseAction = {
    if(!beliefs.gold.contains((x,y)) || beliefs.agentLocations(whom).coordinates!=(x,y)){
      Break
    }else if(places(beliefs.limits).contains(VoidObj(beliefs.alterEgo()))){
      Sense
    }else{
      Move2(x,y).nextAction(beliefs)
    }
  }
}
case object Help{
  def apply(whom:Int,c:(Int,Int)):Help = Help(whom,c._1,c._2)
}

case class Move2(x:Int,y:Int) extends Plan{
  override def nextAction(beliefs: Beliefs): BaseAction = {
    if(beliefs.alterEgo().coordinates == (x,y))
      if(beliefs.loaded && beliefs.status.isAtDepot) Drop
      else Break
    else{
      try {
        val step = beliefs.stepTowards(beliefs.alterEgo().coordinates,(x,y))
        step
      }catch{
        case u:Unreachable =>
          u.printStackTrace()
          beliefs.obstacles.add((x,y))
          Break
      }
    }
  }
}
case object Move2{
  def apply(c:(Int,Int)):Move2 = Move2(c._1,c._2)
}
case class RetrieveGoldFrom(x:Int,y:Int) extends Plan{
  override def nextAction(beliefs: Beliefs): BaseAction = {
    if(beliefs.alterEgo().coordinates == (x,y)){
      if(beliefs.loaded || !beliefs.status.isAtGold){
        Break
      } else if(beliefs.supported) {
        Pick
      }else{
        if(!beliefs.status.isAtGold) println(beliefs.myself+" #########################################################################3")
        Sense
      }
    }else{
      Move2(x,y).nextAction(beliefs)
    }
  }
}
case object RetrieveGoldFrom{
  def apply(c:(Int,Int)):RetrieveGoldFrom = RetrieveGoldFrom(c._1,c._2)
}
